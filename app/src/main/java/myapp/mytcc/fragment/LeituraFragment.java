package myapp.mytcc.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import myapp.mytcc.R;
import myapp.mytcc.activity.AtivoActivity;
import myapp.mytcc.activity.MainActivity;
import myapp.mytcc.helper.ConfigFirebase;
import myapp.mytcc.interfaces.TagListener;
import myapp.mytcc.objects.Ativo;

public class LeituraFragment extends Fragment implements TagListener {

    public static Activity activity;
    private MainActivity mainActivity;


    public LeituraFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_leitura, container, false);

        activity = (Activity) view.getContext();
//        testeLocalizarTag();
        return view;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;

    }


    public void createMock() throws InvocationTargetException, IllegalAccessException, FormatException, NoSuchMethodException {
        NdefMessage ndefMsg = new NdefMessage(new NdefRecord[]{
                new NdefRecord(NdefRecord.TNF_EMPTY, null, null, null)
        });

        Class tagClass = Tag.class;
        Method createMockTagMethod = tagClass.getMethod("createMockTag", byte[].class, int[].class, Bundle[].class);

        final int TECH_NFC_A = 1;
        final int TECH_NDEF = 6;

        final String EXTRA_NDEF_MSG = "ndefmsg";
        final String EXTRA_NDEF_MAXLENGTH = "ndefmaxlength";
        final String EXTRA_NDEF_CARDSTATE = "ndefcardstate";
        final String EXTRA_NDEF_TYPE = "ndeftype";

        Bundle ndefBundle = new Bundle();
        ndefBundle.putInt(EXTRA_NDEF_MSG, 48); // result for getMaxSize()
        ndefBundle.putInt(EXTRA_NDEF_CARDSTATE, 1); // 1: read-only, 2: read/write
        ndefBundle.putInt(EXTRA_NDEF_TYPE, 2); // 1: T1T, 2: T2T, 3: T3T, 4: T4T, 101: MF Classic, 102: ICODE
        ndefBundle.putParcelable(EXTRA_NDEF_MSG, ndefMsg);

        Tag mockTag = (Tag) createMockTagMethod.invoke(null,
                new byte[]{(byte) 0x12, (byte) 0x34, (byte) 0x56, (byte) 0x78},
                new int[]{TECH_NFC_A, TECH_NDEF},
                new Bundle[]{null, ndefBundle});

        Intent techIntent = new Intent(NfcAdapter.ACTION_TECH_DISCOVERED);
        techIntent.putExtra(NfcAdapter.EXTRA_ID, mockTag.getId());
        techIntent.putExtra(NfcAdapter.EXTRA_TAG, mockTag);
        techIntent.putExtra(NfcAdapter.EXTRA_NDEF_MESSAGES, new NdefMessage[]{ndefMsg});
        startActivity(techIntent);

//        final Intent intent = new Intent(NfcAdapter.ACTION_TAG_DISCOVERED);
//        intent.setType("text/plain");
//        intent.putExtra(NfcAdapter.EXTRA_TAG, ndefMsg);
//        startActivity(intent);
    }

    @Override
    public void onTagRead(final Tag tag) {
        final String tagId = bytesToHexString(tag.getId());
        Toast.makeText(activity, "Tag lida: " + tagId, Toast.LENGTH_SHORT).show();
        Log.i("LeituraFragment", "Tag lida: " + tagId);
        if (mainActivity.getTabSelecionada().getClass().equals(LeituraFragment.class)) {

            ConfigFirebase.getFirebaseDataBase().child("Assets").orderByChild("tag_id").equalTo(tagId).addListenerForSingleValueEvent(new ValueEventListener() {
                boolean temAtivoCadastrado = false;

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot dados : dataSnapshot.getChildren()) {
                        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
                        dados.getKey();
                        Ativo ativo = dados.getValue(Ativo.class);
                        Intent intent = new Intent(getActivity(), AtivoActivity.class);
                        intent.putExtra("ativo", ativo);
                        temAtivoCadastrado = true;

                        startActivity(intent);
                       return;
                    }
                    if (!temAtivoCadastrado) {
                        showDialogSemAtivoCadastrado(tagId);
                    }


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


    }

    private void showDialogSemAtivoCadastrado(String idTag) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.app_name);
        builder.setMessage(activity.getResources().getString(R.string.tag_nao_cadastrada).concat(" id: ").concat(idTag));
        builder.setPositiveButton("ok", null);
        builder.create().show();

    }


    private void testeLocalizarTag() {
        if (mainActivity.getTabSelecionada().getClass().equals(LeituraFragment.class)) {
            ConfigFirebase.getFirebaseDataBase().child("Assets").orderByChild("tag_id").equalTo("0x048030aa204e80").addListenerForSingleValueEvent(new ValueEventListener() {
                boolean temAtivoCadastrado = false;

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dataSnapshot.getKey();
                    dataSnapshot.getValue();
                    for (DataSnapshot dados : dataSnapshot.getChildren()) {
                        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
                        dados.getKey();
                        Ativo ativo = dados.getValue(Ativo.class);
                        Intent intent = new Intent(getActivity(), AtivoActivity.class);
                        intent.putExtra("ativo", ativo);
                        temAtivoCadastrado = true;

                        startActivity(intent);
                        return;
                    }

                    if (!temAtivoCadastrado) {
                        showDialogSemAtivoCadastrado("0x048030aa204e80");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


    }


    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }


}
