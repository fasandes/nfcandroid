package myapp.mytcc.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import myapp.mytcc.R;
import myapp.mytcc.activity.AtivoActivity;
import myapp.mytcc.activity.MainActivity;
import myapp.mytcc.adapter.AtivoAdapter;
import myapp.mytcc.helper.ConfigFirebase;
import myapp.mytcc.interfaces.TagListener;
import myapp.mytcc.objects.Ativo;

/**
 * A simple {@link Fragment} subclass.
 */
public class GravarFragment extends Fragment implements TagListener {

    @BindView(R.id.txtTagLida)
    TextView txtTagLida;

    @BindView(R.id.linearLeituraTag)
    LinearLayout linearLeituraTag;

    @BindView(R.id.linearTagCarregada)
    LinearLayout linearTagCarregada;

    private Tag tagLido;
    private Ativo ativoSelecionado;

    private ListView listView;
    private ArrayAdapter adapter;
    private ArrayList<Ativo> mListViewItems;

    public static Activity activity;

    private MainActivity mainActivity;


    public GravarFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
//        firebase.addListenerForSingleValueEvent(valueEventListenerFirebase);
    }

    @Override
    public void onStop() {
        super.onStop();
//        firebase.removeEventListener(valueEventListenerFirebase);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gravar, container, false);
        ButterKnife.bind(this, view);
        mListViewItems = new ArrayList<>();

        listView = (ListView) view.findViewById(R.id.lv_portal_fragment);

        adapter = new AtivoAdapter(getActivity(), mListViewItems);
        listView.setAdapter(adapter);
        activity = (Activity) view.getContext();
//        firebase = ConfigFirebase.getFirebaseDataBase().child("Assets");
//        valueEventListenerFirebase = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                mListViewItems.clear();
//
//                for (DataSnapshot dados : dataSnapshot.getChildren()) {
//
//                    Ativo ativo = dados.getValue(Ativo.class);
//
//                    if ((ativo.getTag_id() == null || ativo.getTag_id().isEmpty() || ativo.getTag_id().equals("0"))) {
//
//                        mListViewItems.add(ativo);
//                    }
//                }
//                adapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//            }
//
//        };

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Ativo ativo = mListViewItems.get(position);
                Intent intent = new Intent(getActivity(), AtivoActivity.class);

                if (tagLido != null) {
                    ativoSelecionado = ativo;
                    exibeConfirmacaoGravacao();
                } else {

                    intent.putExtra("ativo", ativo);

                    startActivity(intent);
                }

            }

        });

        init();

        return view;

    }

    private void init() {
        aplicaVisibilidadeLinear();
        carregaListaAtivos();
    }

    private void carregaListaAtivos() {
        mListViewItems.clear();
        ConfigFirebase.getFirebaseDataBase().child("Assets").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot dados : dataSnapshot.getChildren()) {

                    Ativo ativo = dados.getValue(Ativo.class);

                    if ((ativo.getTag_id() == null || ativo.getTag_id().isEmpty() || ativo.getTag_id().equals("0"))) {

                        mListViewItems.add(ativo);
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void aplicaVisibilidadeLinear() {
        if (tagLido != null) {
            linearLeituraTag.setVisibility(View.GONE);
            linearTagCarregada.setVisibility(View.VISIBLE);
        } else {
            linearLeituraTag.setVisibility(View.VISIBLE);
            linearTagCarregada.setVisibility(View.GONE);
        }
    }

    private void exibeConfirmacaoGravacao() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.app_name);
        builder.setMessage(R.string.confirma_gravar_tag);
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GravarFragment.this.validaLogicaGravacao();

            }
        });
        builder.setNegativeButton("Cancelar", null);
        builder.create().show();
    }

    private void validaLogicaGravacao() {

        ConfigFirebase.getFirebaseDataBase().child("Assets").orderByChild("tag_id").equalTo(bytesToHexString(tagLido.getId())).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren().iterator().hasNext()) {
                    confirmaGravarTag();

                } else {
                    gravarTagAoAtivo();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void confirmaGravarTag() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.app_name);
        builder.setMessage(R.string.tag_ja_cadastrada);
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gravarTagAoAtivo();

            }
        });

        builder.setNegativeButton("Cancelar", null);
        builder.create().show();
    }

    private void gravarTagAoAtivo() {
        ConfigFirebase.getFirebaseDataBase().child("Assets").child(ativoSelecionado.getId()).child("tag_id").setValue(bytesToHexString(tagLido.getId()));
        tagLido = null;
        aplicaVisibilidadeLinear();
        carregaListaAtivos();
    }


    @Override
    public void onTagRead(Tag tag) {

        if (mainActivity.getTabSelecionada().getClass().equals(GravarFragment.class)) {
            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
            toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
            tagLido = tag;
            aplicaVisibilidadeLinear();
            txtTagLida.setText(bytesToHexString(GravarFragment.this.tagLido.getId()));
            Toast.makeText(activity, "TAG carregada, selecione o ativo correspondente", Toast.LENGTH_SHORT).show();

        }

    }


    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }
}
//firebase = ConfigFirebase.getFirebase().child("ativos").child(ativoB.getId().toString());

