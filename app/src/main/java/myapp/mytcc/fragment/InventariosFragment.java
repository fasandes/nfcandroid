package myapp.mytcc.fragment;


import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import myapp.mytcc.R;
import myapp.mytcc.activity.InventarioActivity;
import myapp.mytcc.adapter.InventarioAdapter;
import myapp.mytcc.helper.ConfigFirebase;
import myapp.mytcc.objects.Inventario;

/**
 * A simple {@link Fragment} subclass.
 */
//Consulta fragment
//Consutlar as máquinas (ativos)
public class InventariosFragment extends Fragment {

    private ListView listView;
    private ArrayAdapter adapter;
    private ArrayList<Inventario> mListViewItems;

    public static Activity activity;

    private DatabaseReference firebase;
    private ValueEventListener valueEventListenerFirebase;

    public InventariosFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        firebase.addListenerForSingleValueEvent(valueEventListenerFirebase);
    }

    @Override
    public void onStart() {
        super.onStart();
        firebase.addListenerForSingleValueEvent(valueEventListenerFirebase);
    }

    @Override
    public void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListenerFirebase);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        mListViewItems = new ArrayList<>();

        View view = inflater.inflate(R.layout.fragment_inventario, container, false);

        listView = (ListView) view.findViewById(R.id.lv_portal_fragment);

        adapter = new InventarioAdapter(getActivity(), mListViewItems);

        listView.setAdapter(adapter);

        firebase = ConfigFirebase.getFirebaseDataBase().child("inventarios");

        valueEventListenerFirebase = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mListViewItems.clear();

                for (DataSnapshot dados : dataSnapshot.getChildren()) {

                    Inventario inventario = dados.getValue(Inventario.class);

                    if (!inventario.getBaixa()) {

                        mListViewItems.add(inventario);
                    }

                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        };

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Inventario inventario = mListViewItems.get(position);
                Intent intent = new Intent(getActivity(), InventarioActivity.class);
                intent.putExtra("inventario", inventario);
                startActivity(intent);

            }

        });


        return view;

    }


}
//firebase = ConfigFirebase.getFirebase().child("ativos").child(ativoB.getId().toString());

