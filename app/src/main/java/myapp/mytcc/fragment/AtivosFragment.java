package myapp.mytcc.fragment;


import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import myapp.mytcc.R;
import myapp.mytcc.activity.AtivoActivity;
import myapp.mytcc.adapter.AtivoAdapter;
import myapp.mytcc.helper.ConfigFirebase;
import myapp.mytcc.objects.Ativo;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
//Consulta fragment
//Consutlar as máquinas (ativos)
public class AtivosFragment extends Fragment {

    private ListView listView;
    private ArrayAdapter adapter;
    private ArrayList<Ativo> mListViewItems;

    public static Activity activity;

    private DatabaseReference firebase;
    private ValueEventListener valueEventListenerFirebase;

    public AtivosFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        firebase.addListenerForSingleValueEvent(valueEventListenerFirebase);
    }

    @Override
    public void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListenerFirebase);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        mListViewItems = new ArrayList<>();

        View view = inflater.inflate(R.layout.fragment_ativos, container, false);

        listView = (ListView) view.findViewById(R.id.lv_portal_fragment);

        adapter = new AtivoAdapter(getActivity(), mListViewItems);

        listView.setAdapter(adapter);

        activity = (Activity) view.getContext();

        firebase = ConfigFirebase.getFirebaseDataBase().child("Assets");
        valueEventListenerFirebase = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mListViewItems.clear();

                for (DataSnapshot dados : dataSnapshot.getChildren()) {

                    Ativo ativo = dados.getValue(Ativo.class);

                    mListViewItems.add(ativo);

                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        };

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Ativo ativo = mListViewItems.get(position);
                Intent intent = new Intent(getActivity(), AtivoActivity.class);

                intent.putExtra("ativo", ativo);

                startActivity(intent);

            }

        });


        return view;

    }




}
//firebase = ConfigFirebase.getFirebase().child("ativos").child(ativoB.getId().toString());

