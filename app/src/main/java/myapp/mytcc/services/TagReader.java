package myapp.mytcc.services;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Sandes on 07/11/2017.
 */

public class TagReader implements NfcAdapter.ReaderCallback {

    private Context ctx;

    public TagReader(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public void onTagDiscovered(Tag tag) {
        Log.i("TagReader", "New tag discovered");
        Toast.makeText(ctx, "tag : " + tag.toString(), Toast.LENGTH_SHORT).show();

    }
}
