package myapp.mytcc.interfaces;

import android.nfc.Tag;

/**
 * Created by Sandes on 09/11/2017.
 */

public interface TagListener {

    void onTagRead(Tag tag);
}
