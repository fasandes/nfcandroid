package myapp.mytcc.objects;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sandes on 02/11/2017.
 */

public class Inventario implements Serializable {

    private Boolean baixa;
    private String comentario;
    private String data;
    private String id;
    private String nome;
    private String responsavel;
    private String setor;
    private List<String> ativos;
    private List<String> ativosChecados;

    public Inventario() {
    }

    public Boolean getBaixa() {
        return baixa;
    }

    public void setBaixa(Boolean baixa) {
        this.baixa = baixa;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public List<String> getAtivos() {
        return ativos;
    }

    public void setAtivos(List<String> ativos) {
        this.ativos = ativos;
    }

    public List<String> getAtivosChecados() {
        return ativosChecados;
    }

    public void setAtivosChecados(List<String> ativosChecados) {
        this.ativosChecados = ativosChecados;
    }

}
