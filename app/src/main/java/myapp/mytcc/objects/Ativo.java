package myapp.mytcc.objects;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by gsx0160 on 08/05/2017.
 */

@IgnoreExtraProperties
public class Ativo implements Serializable{

    private String comentario;
    private Long dataAdd;
    private String estado;
    private String id;
    private String marca;
    private String modelo;
    private String nf;
    private String nome;
    private String numero;
    private String responsavel;
    private String setor;
    private String tag_id;
    private String ultimoInventario;



    public Ativo() {
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Long getDataAdd() {
        return dataAdd;
    }

    public void setDataAdd(Long dataAdd) {
        this.dataAdd = dataAdd;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNf() {
        return nf;
    }

    public void setNf(String nf) {
        this.nf = nf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getUltimoInventario() {
        return ultimoInventario;
    }

    public void setUltimoInventario(String ultimoInventario) {
        this.ultimoInventario = ultimoInventario;
    }
}
