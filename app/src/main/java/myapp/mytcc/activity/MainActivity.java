package myapp.mytcc.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import myapp.mytcc.adapter.TabAdapter;
import myapp.mytcc.R;
import myapp.mytcc.helper.ConfigFirebase;
import myapp.mytcc.helper.SlidingTabLayout;
import myapp.mytcc.interfaces.TagListener;

public class MainActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;

    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;
    private TabAdapter tabAdapter;
    private Tag myTag;

    private ArrayList<String> mListViewItems;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference firebase;
    private ValueEventListener valueEventListenerFirebase;
    private static TagListener tagListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("MainActivity", "onCreate");
        setContentView(R.layout.activity_main);
        resolveIntent(getIntent());
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mToolbar.setTitle("Inventário já");
        setSupportActionBar(mToolbar);

        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.stl_tabs);
        viewPager = (ViewPager) findViewById(R.id.vp_pagina);

        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this, R.color.cinzaEscuro));
        tabAdapter = new TabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabAdapter);
        slidingTabLayout.setViewPager(viewPager);


        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.item_sair:
                deslogarUsuario();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    private void deslogarUsuario() {

        firebaseAuth = ConfigFirebase.getFirebaseAutenticacao();
        firebaseAuth.signOut();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();

    }


    public static void resolveIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
//            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
//            NdefMessage[] msgs;
            Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Log.i("MainActivity", "tagListener é null: " + (tagListener == null));
            if (tagListener != null) {
                tagListener.onTagRead(tagFromIntent);
            }

        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        desligaNfcAdapter();
        Log.i("MainActivity", "desligou nfc");
    }

    @Override
    public void onResume() {
        super.onResume();
        ligaNfcAdapter();
        Log.i("MainActivity", "ligou nfc");
    }

    public void ligaNfcAdapter() {
        if (nfcAdapter != null) {
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

    public void desligaNfcAdapter() {
        if (nfcAdapter != null) {
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    public TagListener getTagListener() {
        return tagListener;
    }

    public static void setTagListener(TagListener tagListener) {
        MainActivity.tagListener = tagListener;
    }

    public Fragment getTabSelecionada() {
        return tabAdapter.getItem(viewPager.getCurrentItem());
    }
}