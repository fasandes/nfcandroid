package myapp.mytcc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import myapp.mytcc.R;
import myapp.mytcc.helper.ConfigFirebase;
import myapp.mytcc.objects.Usuario;

public class LoginActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private Usuario usuario;
    private String versionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionName= pInfo.versionName;

        verificarUsuarioLogado();

        final EditText mEditText1 = (EditText) findViewById(R.id.activity_login_text1);
        final EditText mEditText2 = (EditText) findViewById(R.id.activity_login_text2);

        Button mButton = (Button) findViewById(R.id.activity_login_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usuario = new Usuario();
                usuario.setEmail(mEditText1.getText().toString());
                usuario.setSenha(mEditText2.getText().toString());
                validarLogin();

            }
        });


    }

    public void cadastrarUsuario() {

        firebaseAuth = ConfigFirebase.getFirebaseAutenticacao();
        firebaseAuth.createUserWithEmailAndPassword(usuario.getEmail(), usuario.getSenha()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {

                    Toast.makeText(getApplicationContext(), "deu bom", Toast.LENGTH_SHORT).show();

                    FirebaseUser firebaseUser = task.getResult().getUser();
                    usuario.setId(firebaseUser.getUid());
                    usuario.salvar();


                } else

                    Toast.makeText(getApplicationContext(), "deu ruim", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void validarLogin() {

        if ((usuario.getEmail() == null || usuario.getSenha() == null) || (usuario.getEmail().isEmpty() || usuario.getSenha().isEmpty())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.app_name);
            builder.setMessage(R.string.login_usuario_senha_erro);
            builder.setPositiveButton("ok", null);
            builder.create().show();
            return;
        }
        exibirAguarde();

        firebaseAuth = ConfigFirebase.getFirebaseAutenticacao();
        firebaseAuth.signInWithEmailAndPassword(usuario.getEmail(), usuario.getSenha()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {

                    Toast.makeText(LoginActivity.this, "Logado!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(LoginActivity.this, "Versão: " +  versionName, Toast.LENGTH_SHORT).show();
                    LoginActivity.this.progressDialog.dismiss();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();


                } else {
                    LoginActivity.this.progressDialog.dismiss();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Usuário ou senha inválidos", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void verificarUsuarioLogado() {


        firebaseAuth = ConfigFirebase.getFirebaseAutenticacao();

        if (firebaseAuth.getCurrentUser() != null) {

            Toast.makeText(this, "Usuário já esta logado!", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();

        } else Toast.makeText(this, "Favor faça o Login", Toast.LENGTH_LONG).show();
    }

    private void exibirAguarde() {
        this.progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getResources().getString(R.string.login_aguarde_titulo));
        progressDialog.setMessage(getResources().getString(R.string.login_aguarde_mensagem));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

}