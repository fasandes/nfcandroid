package myapp.mytcc.activity;

import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import myapp.mytcc.R;
import myapp.mytcc.objects.Ativo;

public class AtivoActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private String string;
    private Ativo ativo;
    @BindView(R.id.txtNome)
    TextView txtNome;
    @BindView(R.id.txtMarca)
    TextView txtMarca;
    @BindView(R.id.txtModelo)
    TextView txtModelo;
    @BindView(R.id.txtSerialNumber)
    TextView txtSerialNumber;
    @BindView(R.id.txtEstado)
    TextView txtEstado;
    @BindView(R.id.txtResponsavel)
    TextView txtResponsavel;
    @BindView(R.id.txtDepartamento)
    TextView txtDepartamento;
    @BindView(R.id.txtComentario)
    TextView txtComentario;
    @BindView(R.id.txtTag)
    TextView txtTag;
    @BindView(R.id.txtNf)
    TextView txtNf;
    @BindView(R.id.txtDataAquisicao)
    TextView txtDataAquisicao;
    @BindView(R.id.txtInventario)
    TextView txtInventario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ativo);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.portal_toolbar);

        //Puxando os dados passados
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            ativo = (Ativo) extra.getSerializable("ativo");
            initValores(ativo);
        }

        toolbar.setTitle("Ativo");
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AtivoActivity.this.onBackPressed();

            }
        });



    }

    private void initValores(Ativo ativo) {
        Date date = new Date(ativo.getDataAdd());
        SimpleDateFormat formaterDate = new SimpleDateFormat("dd/MM/yyyy");
        this.txtNome.setText(ativo.getNome());
        this.txtMarca.setText(ativo.getMarca());
        this.txtModelo.setText(ativo.getModelo());
        this.txtSerialNumber.setText(ativo.getNumero());
        this.txtEstado.setText(ativo.getEstado());
        this.txtResponsavel.setText(ativo.getResponsavel());
        this.txtDepartamento.setText(ativo.getSetor());
        this.txtComentario.setText(ativo.getComentario());
        this.txtTag.setText(ativo.getTag_id().toString());
        this.txtNf.setText(ativo.getNf());
        this.txtDataAquisicao.setText(formaterDate.format(date));
        this.txtInventario.setText(ativo.getUltimoInventario());



    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();

    }


    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }


}

