package myapp.mytcc.activity;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.OnItemSelected;
import myapp.mytcc.R;
import myapp.mytcc.adapter.AtivoAdapter;
import myapp.mytcc.helper.ConfigFirebase;
import myapp.mytcc.interfaces.TagListener;
import myapp.mytcc.objects.Ativo;
import myapp.mytcc.objects.Inventario;

public class RealizarInventarioActivity extends AppCompatActivity implements TagListener {

    @BindView(R.id.listAtivos)
    ListView listViewAtivos;

    private Inventario inventarioSelecionado;

    private DatabaseReference firebase;
    private AtivoAdapter adapter;
    private ArrayList<Ativo> listaAtivo;
    private ArrayList<Ativo> ativosChecados;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realizar_inventario);
        ButterKnife.bind(this);

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            inventarioSelecionado = (Inventario) extra.getSerializable("inventario");
            init();
        }

        MainActivity.setTagListener(this);


    }

    private void init() {
        listaAtivo = new ArrayList<>();
        ativosChecados = new ArrayList<>();
        adapter = new AtivoAdapter(this, listaAtivo, ativosChecados);
        listViewAtivos.setAdapter(adapter);

        recuperaAtivos();
//
//        new AsyncTask<Void, Void, Void>() {
//
//            @Override
//            protected Void doInBackground(Void... voids) {
//                try {
//                    Looper.prepare();
//                    Thread.sleep(5000);
//                    runOnUiThread(new Runnable() {
//                        public void run() {
//                            testeLerTag();
//                        }
//                    });
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                return null;
//            }
//        }.execute();


    }


    private void recuperaAtivos() {

        ConfigFirebase.getFirebaseDataBase().child("Assets").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dados : dataSnapshot.getChildren()) {
                    Ativo ativo = dados.getValue(Ativo.class);
                    if (inventarioSelecionado.getAtivos().contains(ativo.getId())) {
                        listaAtivo.add(ativo);
                        if (inventarioSelecionado.getAtivosChecados() != null && inventarioSelecionado.getAtivosChecados().contains(ativo.getId())) {
                            ativosChecados.add(ativo);
                        }
                    }
                }

                RealizarInventarioActivity.this.adapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void testaRegistroAtivo() {
        ativosChecados.add(listaAtivo.get(0));
        registarAtivoChecado();
        adapter.notifyDataSetChanged();
    }

    private void registarAtivoChecado() {
        ConfigFirebase.getFirebaseDataBase().child("inventarios").child(inventarioSelecionado.getId()).child("ativosChecados").setValue(getArrayAtivosChecados());
        if (listaAtivo.size() == ativosChecados.size()) {
            ConfigFirebase.getFirebaseDataBase().child("inventarios").child(inventarioSelecionado.getId()).child("baixa").setValue(Boolean.TRUE);
            Toast.makeText(this, "Inventário Realizado com sucesso", Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    private List<String> getArrayAtivosChecados() {
        List<String> arrayChecados = new ArrayList<>();
        for (Ativo ativo : ativosChecados) {
            arrayChecados.add(ativo.getId());
        }

        return arrayChecados;
    }


    @OnItemClick(R.id.listAtivos)
    void onItemSelected(AdapterView<?> parent, int position) {
        listViewAtivos.setItemChecked(position, true);
    }


    @Override
    public void onTagRead(Tag tag) {
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
        String idFormatado = bytesToHexString(tag.getId());
        Toast.makeText(this, "Tag lida" + idFormatado, Toast.LENGTH_SHORT).show();


        for (Ativo ativo : listaAtivo) {
            if (ativo.getTag_id().equals(idFormatado)) {
                ativosChecados.add(ativo);
                adapter.notifyDataSetChanged();
                registarAtivoChecado();
            }
        }

    }

    private void testeLerTag() {
        String id = "0x048030aa204e80";
        Toast.makeText(this, "Tag lida" + id, Toast.LENGTH_SHORT).show();

        for (Ativo ativo : listaAtivo) {
            if (ativo.getTag_id().equals(id)) {
                ativosChecados.add(ativo);
                adapter.notifyDataSetChanged();
                break;
            }
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MainActivity.setTagListener(null);
    }

    public void createMock() throws InvocationTargetException, IllegalAccessException, FormatException, NoSuchMethodException {
        NdefMessage ndefMsg = new NdefMessage(new NdefRecord[]{
                new NdefRecord(NdefRecord.TNF_EMPTY, null, null, null)
        });

        Class tagClass = Tag.class;
        Method createMockTagMethod = tagClass.getMethod("createMockTag", byte[].class, int[].class, Bundle[].class);

        final int TECH_NFC_A = 1;
        final int TECH_NDEF = 6;

        final String EXTRA_NDEF_MSG = "ndefmsg";
        final String EXTRA_NDEF_MAXLENGTH = "ndefmaxlength";
        final String EXTRA_NDEF_CARDSTATE = "ndefcardstate";
        final String EXTRA_NDEF_TYPE = "ndeftype";

        Bundle ndefBundle = new Bundle();
        ndefBundle.putInt(EXTRA_NDEF_MSG, 48); // result for getMaxSize()
        ndefBundle.putInt(EXTRA_NDEF_CARDSTATE, 1); // 1: read-only, 2: read/write
        ndefBundle.putInt(EXTRA_NDEF_TYPE, 2); // 1: T1T, 2: T2T, 3: T3T, 4: T4T, 101: MF Classic, 102: ICODE
        ndefBundle.putParcelable(EXTRA_NDEF_MSG, ndefMsg);

        Tag mockTag = (Tag) createMockTagMethod.invoke(null,
                new byte[]{(byte) 0x12, (byte) 0x34, (byte) 0x56, (byte) 0x78},
                new int[]{TECH_NFC_A, TECH_NDEF},
                new Bundle[]{null, ndefBundle});

        Intent techIntent = new Intent(NfcAdapter.ACTION_TECH_DISCOVERED);
        techIntent.putExtra(NfcAdapter.EXTRA_ID, mockTag.getId());
        techIntent.putExtra(NfcAdapter.EXTRA_TAG, mockTag);
        techIntent.putExtra(NfcAdapter.EXTRA_NDEF_MESSAGES, new NdefMessage[]{ndefMsg});
        startActivity(techIntent);

//        final Intent intent = new Intent(NfcAdapter.ACTION_TAG_DISCOVERED);
//        intent.setType("text/plain");
//        intent.putExtra(NfcAdapter.EXTRA_TAG, ndefMsg);
//        startActivity(intent);
    }

    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }

}
