package myapp.mytcc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import myapp.mytcc.R;
import myapp.mytcc.adapter.AtivoAdapter;
import myapp.mytcc.helper.ConfigFirebase;
import myapp.mytcc.objects.Ativo;
import myapp.mytcc.objects.Inventario;


/**
 * Created by Victor on 08/06/2017.
 */

public class InventarioActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Inventario inventario;
    private AlertDialog.Builder builder;

    @BindView(R.id.txtNome)
    TextView txtNome;
    @BindView(R.id.txtResponsavel)
    TextView txtResponsavel;
    @BindView(R.id.txtDepartamento)
    TextView txtDepartamento;
    @BindView(R.id.txtDataAquisicao)
    TextView txtDataAquisicao;
    @BindView(R.id.txtComentario)
    TextView txtComentario;
    @BindView(R.id.txtAtivos)
    TextView txtAtivos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.portal_toolbar);

        //Puxando os dados passados
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            inventario = (Inventario) extra.getSerializable("inventario");
            initValores(inventario);
        }
        toolbar.setTitle("Inventário");
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InventarioActivity.super.onBackPressed();

            }
        });


    }

    private void initValores(Inventario inventario) {
        Date date = new Date(inventario.getData());
        SimpleDateFormat formaterDate = new SimpleDateFormat("dd/MM/yyyy");
        this.txtNome.setText(inventario.getNome());
        this.txtComentario.setText(inventario.getComentario());
        this.txtDataAquisicao.setText(formaterDate.format(date));
        this.txtDepartamento.setText(inventario.getSetor());
        this.txtResponsavel.setText(inventario.getResponsavel());
        Integer quantidadeAtivos = 0;
        if (inventario.getAtivos() != null) {
            quantidadeAtivos = inventario.getAtivos().size();
        }
        this.txtAtivos.setText(quantidadeAtivos.toString());
    }

    @OnClick(R.id.txtAtivos)
    void exibeAtivosDialog() {
        final ArrayList<Ativo> listaAtivos = new ArrayList<Ativo>();


        builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_ativos, null);

        ListView listView = (ListView) view.findViewById(R.id.lv_portal_fragment);

        final AtivoAdapter adapter = new AtivoAdapter(this, listaAtivos);

        listView.setAdapter(adapter);

        if (inventario.getAtivos() != null) {
            ConfigFirebase.getFirebaseDataBase().child("Assets").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listaAtivos.clear();

                    for (DataSnapshot dados : dataSnapshot.getChildren()) {

                        if (inventario.getAtivos().contains(dados.getKey())) {
                            Ativo ativo = dados.getValue(Ativo.class);

                            listaAtivos.add(ativo);
                        }

                    }

                    adapter.notifyDataSetChanged();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        builder.setView(view);
        builder.create();
        builder.show();


    }

    @OnClick(R.id.fabRealizarInventario)
    void realizarInventario() {

        Intent it = new Intent(this,RealizarInventarioActivity.class);
        it.putExtra("inventario", inventario);
        startActivity(it);
        finish();

    }


}
