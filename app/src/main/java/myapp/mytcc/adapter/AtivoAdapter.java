package myapp.mytcc.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import myapp.mytcc.R;
import myapp.mytcc.objects.Ativo;
import myapp.mytcc.objects.Inventario;

public class AtivoAdapter extends ArrayAdapter<Ativo> {

    private ArrayList<Ativo> ativos;
    private ArrayList<Ativo> ativosChecados;
    private Context context;


    public AtivoAdapter(Context ctx, ArrayList<Ativo> objects, ArrayList<Ativo> ativosChecados) {
        super(ctx, 0, objects);
        this.ativos = objects;
        this.context = ctx;
        this.ativosChecados = ativosChecados;
    }

    public AtivoAdapter(Context ctx, ArrayList<Ativo> objects) {
        super(ctx, 0, objects);
        this.ativos = objects;
        this.context = ctx;
        this.ativosChecados = null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;

        if (ativos != null) {
            LinearLayout linearListaLayoutChecked = null;

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            if (ativosChecados != null) {
                view = layoutInflater.inflate(R.layout.lista_layout_checked, parent, false);
                linearListaLayoutChecked = (LinearLayout) view.findViewById(R.id.linearListaLayoutChecked);

            } else {
                view = layoutInflater.inflate(R.layout.lista_layout, parent, false);
            }


            TextView mTextViewNome = (TextView) view.findViewById(R.id.lista_layout_textview_nome);

            ImageView mImageViewImagem = (ImageView) view.findViewById(R.id.lista_layout_imageview_imagem);


            Ativo ativo = ativos.get(position);

            mTextViewNome.setText(ativo.getNome());

            if (ativosChecados != null && ativosChecados.contains(ativo)) {
                linearListaLayoutChecked.setBackground(context.getDrawable(R.drawable.border_checked));

            }

            if (ativo.getNome().toUpperCase().contains("PRIN")) {
                mImageViewImagem.setImageResource(R.drawable.ic_local_printshop);
            }

            if (ativo.getNome().toUpperCase().contains("NOTE")) {
                mImageViewImagem.setImageResource(R.drawable.ic_laptop_chromebook);
            }

            if (ativo.getNome().toUpperCase().contains("DESK")) {
                mImageViewImagem.setImageResource(R.drawable.ic_desktop_mac);
            }


            view.setTag(ativo);

        }

        return view;

    }

    public void checkaAtivo(Ativo ativo) {
    }
}
