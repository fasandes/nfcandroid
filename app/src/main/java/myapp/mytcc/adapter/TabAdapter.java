package myapp.mytcc.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import myapp.mytcc.activity.MainActivity;
import myapp.mytcc.fragment.AtivosFragment;
import myapp.mytcc.fragment.GravarFragment;
import myapp.mytcc.fragment.InventariosFragment;
import myapp.mytcc.fragment.LeituraFragment;
import myapp.mytcc.interfaces.TagListener;

/**
 * Created by Matheus on 01/05/2017.
 */

public class TabAdapter extends FragmentStatePagerAdapter {

    public static final String[] TITULO_ABAS = {"Ativos", "Inventarios", "Gravar", "Leitura"};

    private Fragment mCurrentFragment;


    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    public Fragment getmCurrentFragment() {
        return mCurrentFragment;
    }

    public void setmCurrentFragment(Fragment mCurrentFragment) {
        this.mCurrentFragment = mCurrentFragment;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position) {

            case 0:
                fragment = new AtivosFragment();
                break;

            case 1:
                fragment = new InventariosFragment();
                break;

            case 2:
                fragment = new GravarFragment();
                break;

            case 3:
                fragment = new LeituraFragment();
                break;

        }

        return fragment;

    }

    @Override
    public int getCount() {
        return TITULO_ABAS.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITULO_ABAS[position];
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getmCurrentFragment() != object) {
            mCurrentFragment = ((Fragment) object);

            Log.i("TabAdapter","Current fragment : " + mCurrentFragment.getClass().toString());
            if (mCurrentFragment instanceof TagListener) {
                MainActivity.setTagListener((TagListener) mCurrentFragment);
            }
        }
        super.setPrimaryItem(container, position, object);
    }

    enum AbasInstancia {
        ativos(0, AtivosFragment.class),
        inventarios(1, InventariosFragment.class),
        gravar(2, GravarFragment.class),
        leitura(3, LeituraFragment.class);

        private int pos;
        private Class fragment;

        AbasInstancia(int pos, Class fragment) {
            this.pos = pos;
            this.fragment = fragment;

        }

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

        public Class getFragment() {
            return fragment;
        }

        public void setFragment(Class fragment) {
            this.fragment = fragment;
        }
    }
}
