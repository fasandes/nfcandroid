package myapp.mytcc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import myapp.mytcc.R;
import myapp.mytcc.objects.Ativo;
import myapp.mytcc.objects.Inventario;

public class InventarioAdapter extends ArrayAdapter<Inventario> {

    private ArrayList<Inventario> inventarios;
    private Context context;

    public InventarioAdapter(Context ctx, ArrayList<Inventario> objects) {
        super(ctx, 0, objects);
        this.inventarios = objects;
        this.context = ctx;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;

        if(inventarios != null){

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            view = layoutInflater.inflate(R.layout.lista_layout, parent, false);

            TextView mTextViewNome = (TextView) view.findViewById(R.id.lista_layout_textview_nome);

            ImageView mImageViewImagem = (ImageView) view.findViewById(R.id.lista_layout_imageview_imagem);

            Inventario inventario = inventarios.get(position);

            mTextViewNome.setText(inventario.getNome());



        }

        return view;

    }
}
