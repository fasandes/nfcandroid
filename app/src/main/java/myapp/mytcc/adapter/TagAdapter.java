package myapp.mytcc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import myapp.mytcc.R;

public class TagAdapter extends ArrayAdapter<String> {

    public TagAdapter(Context context, ArrayList<String> mListView) {
        super(context, R.layout.adapter_tag, mListView);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());

        View view = layoutInflater.inflate(R.layout.adapter_tag, parent, false);

        String mListViewItem = getItem(position);

        TextView textView = (TextView) view.findViewById(R.id.adapter_tag_textview2);

        textView.setText(mListViewItem);

        return view;

    }
}
