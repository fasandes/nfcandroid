package myapp.mytcc.helper;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public final class ConfigFirebase {

    private static DatabaseReference databaseReference;
    private static FirebaseAuth firebaseAuth;

    public static DatabaseReference getFirebaseDataBase(){

        if( databaseReference == null ){
            databaseReference = FirebaseDatabase.getInstance().getReference();
        }

        return databaseReference;
    }

    public static FirebaseAuth getFirebaseAutenticacao(){

        if( firebaseAuth == null ){

            firebaseAuth = FirebaseAuth.getInstance();

        }
        return firebaseAuth;
    }

}
